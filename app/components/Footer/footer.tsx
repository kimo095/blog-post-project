import React, { ReactNode } from 'react';

interface FooterProps {
  children?: ReactNode;
  className?: string;
}

const Footer: React.FC<FooterProps> = ({ children }) => {
  const currentYear = new Date().getFullYear();
  
  return (
    <footer className="fixed bottom-0 left-0 w-full bg-gray-800 text-white text-center" style={{ height: '32px' }}>
      <p className='text-sm text-center pt-1.5'>
        {children ? children : 'Social Brothers'} - {currentYear}
      </p>
    </footer>
  );
}

export default Footer;
