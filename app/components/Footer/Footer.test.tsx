import { render, screen } from '@testing-library/react';
import Footer from './footer';

describe('Footer', () => {
    it('renders default text "Social Brothers" correctly when no children are provided', () => {
        render(<Footer />); // ARRANGE

        const footerContent = screen.getByText(/Social Brothers/i); // ACT

        expect(footerContent).toBeInTheDocument(); // ASSERT
    });
    it('renders custom children text correctly', () => {
        const customText = 'Custom Text';
        render(<Footer>{customText}</Footer>); // ARRANGE

        const footerContent = screen.getByText(/Custom Text/i); // ACT

        expect(footerContent).toBeInTheDocument(); // ASSERT
    });
    it('renders the current year correctly', () => {
        const currentYear = new Date().getFullYear();
        const originalDate = Date;
        global.Date = jest.fn(() => new originalDate('2024-01-01T00:00:00Z')) as any;

        render(<Footer />); // ARRANGE

        const footerContent = screen.getByText(new RegExp(currentYear.toString(), 'i')); // ACT

        expect(footerContent).toBeInTheDocument(); // ASSERT

        // Restore the original Date constructor
        global.Date = originalDate;
    });
});
