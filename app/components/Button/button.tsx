interface Button {
  onClick?: () => void;
  buttonStyle?: React.CSSProperties;
  children?: React.ReactNode;
  type?: "button" | "submit" | "reset";
  className?:string;
}

const Button: React.FC<Button> = ({ onClick, buttonStyle, children }) => {
  return (
    <div className="text-center mt-8">
      <button onClick={onClick} className="blog-button" style={{ width: '200px', ...buttonStyle }}>
        {children}
      </button>
    </div>
  );
}

export default Button;
