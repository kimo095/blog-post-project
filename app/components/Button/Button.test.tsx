import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from './button';

describe('Button component', () => {
    it('renders button with correct children and style', () => {
        // Render the Button component
        const { getByText } = render(
            <Button buttonStyle={{ color: 'red' }} onClick={() => {}}>
                Click me
            </Button>
        );

        // Assert that the button is rendered with correct children and style
        const buttonElement = getByText('Click me');
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement.tagName).toBe('BUTTON');
        expect(buttonElement).toHaveStyle('color: red');
    });

    it('calls onClick function when clicked', () => {
        // Mock onClick function
        const onClickMock = jest.fn();

        // Render the Button component
        const { getByText } = render(
            <Button onClick={onClickMock}>
                Click me
            </Button>
        );

        // Click the button
        const buttonElement = getByText('Click me');
        fireEvent.click(buttonElement);

        // Verify that onClick function is called
        expect(onClickMock).toHaveBeenCalled();
    });
});
