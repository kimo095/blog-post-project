import { PaginationProps } from "../../interferances/interferance";

const Pagination: React.FC<PaginationProps> = ({ currentPage, totalPages, onPageChange }) => {
    const handlePageChange = (pageNumber: number | string) => {
        if (typeof pageNumber === 'number' && totalPages !== undefined && pageNumber >= 1 && pageNumber <= totalPages) {
            onPageChange(pageNumber);
        }
    };
    
    const renderPageNumbers = () => {
        const adjacentPages = 2; // Number of adjacent pages to display around the current page
        const pageNumbers = [];
    
        // First page
        pageNumbers.push(1);
    
        // Previous ellipsis if currentPage is more than adjacentPages + 2
        if (currentPage > adjacentPages + 2) {
            pageNumbers.push("...");
        }
    
        // Previous adjacent pages
        for (let i = Math.max(2, currentPage - adjacentPages); i < currentPage; i++) {
            pageNumbers.push(i);
        }
    
        // Current page
        if (!pageNumbers.includes(currentPage)) {
            pageNumbers.push(currentPage);
        }
    
        // Next adjacent pages
        if (totalPages !== undefined) {
            for (let i = currentPage + 1; i <= Math.min(currentPage + adjacentPages, totalPages); i++) {
                pageNumbers.push(i);
            }
    
            // Next ellipsis if totalPages is more than currentPage + adjacentPages + 1
            if (totalPages > currentPage + adjacentPages + 1) {
                pageNumbers.push("...");
            }
            // Last page
            if (totalPages > currentPage + adjacentPages) {
                pageNumbers.push(totalPages);
            }
        }
    
    
        return pageNumbers.map((number, index) => (
            <a
                key={index}
                className={` ${currentPage === number ? 'active flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white rounded-full font-bold' : ''}`}
                onClick={() => handlePageChange(typeof number === 'number' ? number : currentPage)}>
                {number}
            </a>
        ));
    };
    
    return (
        <div className="mb-12 flex justify-between items-center auto" >
            <div className="flex" style={{ width: '322px' , height:"25px" }} >
                {renderPageNumbers()}
            </div>
            <div className=" ml-2 h-19 w-95 text-orange-500 font-inter text-xs leading-4 cursor-pointer" onClick={() => handlePageChange(currentPage + 1)}>
                Next page
            </div>
        </div>
    );
};

export default Pagination;
