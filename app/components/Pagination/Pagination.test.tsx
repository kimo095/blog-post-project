import React from 'react';
import { render, fireEvent  } from '@testing-library/react';
import Pagination from './pagination';

describe('Pagination Component', () => {
    it('should call onPageChange with the next page number when a pagination link is clicked', () => {
        const onPageChangeMock = jest.fn();
        const totalPages = 5;
        const currentPage = 2;

        const { getByText } = render(
            <Pagination
                currentPage={currentPage}
                totalPages={totalPages}
                onPageChange={onPageChangeMock}
            />
        );

        const nextPageNumber = currentPage + 1;

        const nextPageLink = getByText(nextPageNumber.toString());

        fireEvent.click(nextPageLink);

        expect(onPageChangeMock).toHaveBeenCalledWith(nextPageNumber);
    });
    it('should call onPageChange with the next page number when a pagination link is clicked', () => {
        const onPageChangeMock = jest.fn();
        const totalPages = 5;
        const currentPage = 2;

        const { getByText } = render(
            <Pagination
                currentPage={currentPage}
                totalPages={totalPages}
                onPageChange={onPageChangeMock}
            />
        );

        const nextPageNumber = currentPage + 1;

        const nextPageLink = getByText(nextPageNumber.toString());

        fireEvent.click(nextPageLink);

        expect(onPageChangeMock).toHaveBeenCalledWith(currentPage + 1);
        expect(currentPage + 1).toBe(3);
    });
});