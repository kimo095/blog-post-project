import { BlogCardProps } from "../../interferances/interferance";

const BlogCard: React.FC<BlogCardProps> = ({ img_url, title, content, useSpecialStyle }) => {
    return (
        <div className='border-b hover:shadow-lg'>
        <img className={`shadow object-cover ${useSpecialStyle ? 'h-20 w-72' : 'h-20 w-64  '}`} src={`https://frontend-case-api.sbdev.nl/storage/${img_url}`} alt={title} />
        <div className="m-4">
        <span className={`inline-block mb-1 text-xl font-bold  w-40 text-gray-700 leading-7`}>{title}</span>
        <span className={`inline-block mb-1 text-sm  w-40 text-gray-500  leading-7`}>{content}</span>
        </div>
        </div>
    );
}
export default BlogCard
