import React, { useState } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation'; 
import { AppBar, CssBaseline, ThemeProvider, createTheme } from '@mui/material';
import { DarkModeProvider, useDarkMode } from '@/app/DarkModeContext';

const Header: React.FC<{ showBlog?: boolean }> = ({ showBlog = true }) => {
  const pathname = usePathname();
  const { darkMode, toggleDarkMode } = useDarkMode();
  const isHomeActive = pathname === '/';
  const isBlogActive = pathname === '/blogs';

  const [menuOpen, setMenuOpen] = useState<boolean>(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  const theme = createTheme({
    palette: {
      mode: darkMode ? 'dark' : 'light',
    },
  });

  return (
    <main className="mask fixed">
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar position="static">
          <button onClick={toggleDarkMode} className="btn-fake-dark-mode fixed top-0 right-0 p-4 leading-none text-2xl bg-yellow-200 border-none">
            {darkMode ? "☀️" : "🌙"}
          </button>
        </AppBar>
        {showBlog && <h1 className="absolute mt-36 transform -translate-x-1/2 h-16 w-96 left-1/2 text-white font-inter text-4xl font-bold leading-16 text-center">Blogs</h1>} 
            <div className="absolute mt-12 ml-4">
              <Link href="/">
                <img src="/logo.svg" alt="logo" style={{ width: '700px', height: '57px' }} />
              </Link>
            </div>
        <nav>
          <button onClick={toggleMenu} className="flex justify-end burger-menu md:hidden">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-16 h-12 text-white">
              <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
            </svg>
          </button>
          <div className={`absolute right-40 mt-10 ml-12 ${menuOpen ? 'sm:hidden' : 'block'}`}>
          <ul className='md:flex md:justify-start w-52 '>
            <li className={`pb-3 text-xl text-white py-2 md:px-6 text-center ${isHomeActive ? 'border-b-4 border-orange-500' : ''}`}>
              <Link href="/">Home</Link>
            </li>
            <li className={`pb-3 text-xl text-white py-2 md:px-6 text-center ${isBlogActive ? 'border-b-4 border-orange-500' : ''}`}>
              <Link href="/blogs">Blog</Link>
            </li>
          </ul>
        </div>

        </nav> 
      </ThemeProvider>
    </main>
  );
};

const HeaderWithDarkModeProvider: React.FC<{ showBlog?: boolean }> = ({ showBlog }) => {
  return (
    <DarkModeProvider>
      <Header showBlog={showBlog} />
    </DarkModeProvider>
  );
};

export default HeaderWithDarkModeProvider;

