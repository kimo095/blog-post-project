import {render ,screen } from '@testing-library/react';

import Header from './header';

describe('Header' , ()=> {

    it('renders Home link text correctly', () => {
        render(<Header />); //ARRANGE

        const homeLinkListItem = screen.getByText('Home').closest('li'); // ACT
        
        expect(homeLinkListItem).toBeInTheDocument(); //ASSERT
      });
    
      it('renders Blog link text correctly', () => {
        render(<Header />);

        const blogLinkListItem = screen.getByText('Blog').closest('li'); // ACT
        expect(blogLinkListItem).toBeInTheDocument(); //ASSERT
    });

      
    it('renders Blog header correctly when showBlog is true', () => {
        render(<Header showBlog={true} />);
        
        const blogHeader = screen.getByRole('heading', { name: /Blogs/i });
        
        expect(blogHeader).toBeInTheDocument();
      })

    
})
