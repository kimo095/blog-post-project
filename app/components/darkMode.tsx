import React, { useState, useEffect } from 'react';

const DarkMode: React.FC = () => {
  const [isFakeDark, setIsFakeDark] = useState(false);

  useEffect(() => {
    document.documentElement.classList.toggle("fake-dark-mode", isFakeDark);
  }, [isFakeDark]);

  const toggleDarkMode = () => {
    setIsFakeDark(prevIsFakeDark => !prevIsFakeDark);
  };

  return (
    <div>
      <button
        onClick={toggleDarkMode}
        className="btn-fake-dark-mode"
      >
        {isFakeDark ? "☀️" : "🌙"}
      </button>
    </div>
  );
};

export default DarkMode;
