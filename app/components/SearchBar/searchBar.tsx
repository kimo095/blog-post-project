import React from 'react';

interface SearchInputProps {
    setSearchQuery: (query: string) => void;
    searchQuery: string;
    showSearchBar?: boolean;
}

const SearchInput: React.FC<SearchInputProps> = ({ setSearchQuery, searchQuery }) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchQuery(e.target.value);
    };

    return (
        <div className='flex justify-center items-center mt-10'>
            <input
                className="py-2 px-4 border  bg-slate-50 border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500"
                value={searchQuery}
                onChange={handleChange}
                placeholder="Search by title..."
            />
        </div>
    );
}

export default SearchInput;
