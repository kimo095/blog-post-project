import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import SearchInput from './searchBar';

describe('SearchInput', () => {
    test('should update search query correctly', () => {
        // Mock setSearchQuery function
        const setSearchQueryMock = jest.fn();

        // Render the SearchInput component
        const { getByPlaceholderText } = render(
            <SearchInput setSearchQuery={setSearchQueryMock} searchQuery="" />
        );

        // Get the input element
        const inputElement = getByPlaceholderText('Search by title...');

        // Simulate user input
        fireEvent.change(inputElement, { target: { value: 'test' } });

        // Verify that setSearchQuery function is called with the correct search query value
        expect(setSearchQueryMock).toHaveBeenCalledWith('test');
    });
});
