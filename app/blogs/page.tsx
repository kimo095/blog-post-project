'use client';
import React, { useState, lazy, Suspense } from 'react';
import Footer from '../components/Footer/footer';
import Pagination from '../components/Pagination/pagination';
import { Blog } from '../interferances/interferance';
import LoadingSpinner from '../components/loadingSpinner';
import HeaderWithDarkModeProvider from '../components/Header/header';

const Getblogs = lazy(() => import('../Data-fetch/GetBlogs/getBlogs'));

const Blogs: React.FC = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [blogs, setBlogs] = useState<Blog[]>([]);
    const [totalPages, setTotalPages] = useState<number>(1); // Initialize totalPages state

    const paginate = (pageNumber: number) => {
        setCurrentPage(pageNumber);
    };

    return (
     
            <Suspense fallback={<LoadingSpinner />}>
                <main>
            <HeaderWithDarkModeProvider/>
                    <div className="p-6 mt-4" style={{ width: '1200px', height: '800px' }}>
                        <Getblogs currentPage={currentPage} blogs={blogs} setBlogs={setBlogs} useSpecialStyle={false} showSearchBar={true} onLastPageChange={setTotalPages} /> {/* Pass onLastPageChange callback */}
                        <div className="flex justify-center  mt-10">
                            <Pagination currentPage={currentPage} totalPages={totalPages} onPageChange={paginate} />
                        </div>
                    </div>
                    <Footer />
                </main>
            </Suspense>
    );
}

export default Blogs;












