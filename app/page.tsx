'use client';
import React, { useState, useMemo, lazy, Suspense } from 'react';
import LoadingSpinner from './components/loadingSpinner';
import { Blog } from './interferances/interferance';
import HeaderWithDarkModeProvider from './components/Header/header';

const Header = lazy(() => import("./components/Header/header"));
const Getblogs = lazy(() => import('./Data-fetch/GetBlogs/getBlogs'));
const Button = lazy(() => import('./components/Button/button'));
const Footer = lazy(() => import('./components/Footer/footer'));
const AddBlogs = lazy(() => import('./Data-fetch/AddBlogs/addBlogs'));

export default function HomePage() {
  const [numInitialBlogs, setNumInitialBlogs] = useState(4);
  const [blogs, setBlogs] = useState<Blog[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  

  const displayedBlogs = useMemo(() => {
    return blogs.slice(0, numInitialBlogs);
  }, [blogs, numInitialBlogs]);

  const loadMoreBlogs = () => {
    const newNumInitialBlogs = numInitialBlogs + 8;
    setNumInitialBlogs(newNumInitialBlogs);
  };



  return (
        <Suspense fallback={<LoadingSpinner />}>
        <HeaderWithDarkModeProvider showBlog={false}/>
          <main className="flex justify-between mb-20" style={{ width: '1117px' , height: 'auto'}}>
         <div className="mr-6 mt-8" >
         <AddBlogs />
         </div>
         <div className='flex flex-col mt-8 p-4 bg-white' style={{ width: '642px'}}>
            <Getblogs  currentPage={currentPage} blogs={displayedBlogs} setBlogs={setBlogs} useSpecialStyle={true}  showSearchBar={false} onLastPageChange={setTotalPages} />
            <div className='mt-36'>
            {numInitialBlogs < blogs.length && (
              <Button  onClick={loadMoreBlogs}>Load More</Button>
              )}
            </div>
          </div>
              </main>
          <Footer />
        </Suspense>
  );
}
