import React from 'react';
import { render, act } from '@testing-library/react';
import Getblogs from './getBlogs';

interface Blog {
    id: number;
    img_url: string;
    title: string;
    content: string;
}

describe('Getblogs ', () => {
    it('should call setBlogs with fetched data', async () => {
        const setBlogsMock = jest.fn();
        const currentPage = 1;
        const blogs: Blog[] = [];
        const useSpecialStyle = false;

        // Mocking fetch function
        global.fetch = jest.fn().mockResolvedValueOnce({
            ok: true,
            json: async () => ({ data: [] })
        } as any);

        await act(async () => {
            render(
                <Getblogs
                    currentPage={currentPage}
                    blogs={blogs}
                    setBlogs={setBlogsMock}
                    useSpecialStyle={useSpecialStyle}
                />
            );
        });

        expect(setBlogsMock).toHaveBeenCalled();
    });
});
