'use client';
import React, { useEffect, useState } from 'react';
import BlogCard from '../../components/BlogCard/blogcard';
import { GetBlogsProps } from '../../interferances/interferance';
import SearchInput from '@/app/components/SearchBar/searchBar';

const Getblogs: React.FC<GetBlogsProps & { showSearchBar?: boolean; onLastPageChange: (lastPage: number) => void }> = ({ currentPage, blogs: initialBlogs, setBlogs, useSpecialStyle, showSearchBar = true, onLastPageChange }) => {
    const [isLoading, setIsLoading] = useState(false);
    const [searchQuery, setSearchQuery] = useState("");
    const [blogs, setFilteredBlogs] = useState(initialBlogs);
    const [lastPage, setLastPage] = useState(1); // Initialize lastPage state

    const API_TOKEN = 'pj11daaQRz7zUIH56B9Z';
    const BASE_URL = "https://frontend-case-api.sbdev.nl";

    useEffect(() => {
        // Fetch data from the API
        async function fetchData() {
            setIsLoading(true);
            try {
                const res = await fetch(`${BASE_URL}/api/posts?page=${currentPage}`, {
                    headers: {
                        Token: API_TOKEN
                    }
                });
                if (!res.ok) {
                    throw new Error('Failed to fetch data');
                }
                const data = await res.json();
                if (setBlogs) setBlogs(data.data); // Check if setBlogs is defined before calling
                setLastPage(data.last_page); // Set last page
                onLastPageChange(data.last_page); // Notify parent component about last page change
            } catch (error) {
                console.error('Error fetching data:', error);
            } finally {
                setIsLoading(false);
            }
        }
        fetchData();
    }, [currentPage, setBlogs, onLastPageChange]); // Add onLastPageChange to the dependency array

    // Filter blogs based on search query
    useEffect(() => {
        const filtered = initialBlogs.filter(blog =>
            blog.title.toLowerCase().includes(searchQuery.toLowerCase())
        );
        setFilteredBlogs(filtered);
    }, [initialBlogs, searchQuery]);

    return (
        <div>

            {showSearchBar &&
                <div className='mb-6'>
                    <SearchInput searchQuery={searchQuery} setSearchQuery={setSearchQuery} />
                </div>
            }

            <div className={useSpecialStyle ? 'gap-4 grid lg:grid-cols-2' : 'md:gap-1 grid md:grid-cols-2 lg:grid-cols-4 lg:gab-4 '}>
                {isLoading ? (
                    <p>Loading...</p>
                ) : (
                    blogs.map(blog => (
                        <BlogCard
                            key={blog.id}
                            id={blog.id}
                            img_url={blog.img_url}
                            title={blog.title}
                            content={blog.content}
                            useSpecialStyle={useSpecialStyle}
                        />
                    ))
                )}
            </div>
        </div>
    );
}

export default Getblogs;


