// 'use client' (not valid in React components)
import React, { useState } from 'react';
import Button from '../../components/Button/button';
import { useForm } from 'react-hook-form';
import { PostFormData } from '../../interferances/interferance';

const AddBlogs: React.FC = () => {
    const { register, handleSubmit, formState: { errors }, setValue } = useForm<PostFormData>();
    const [selectedCategory, setSelectedCategory] = useState(""); 
    const API_TOKEN = 'pj11daaQRz7zUIH56B9Z'; 
    const BASE_URL = "https://frontend-case-api.sbdev.nl";

    const onSubmit = async (data: PostFormData) => {
        try {
            const formDataToSend = new FormData();
            formDataToSend.append('title', data.title);
            formDataToSend.append('content', data.content);
            formDataToSend.append('category_id', selectedCategory); 
            if (data.image) {
                formDataToSend.append('image', data.image[0]);
            }

            const response = await fetch(`${BASE_URL}/api/posts`, {
                method: 'POST',
                headers: {
                    'Token': API_TOKEN,
                },
                body: formDataToSend,
            });

            if (response.ok) {
                alert('Post submitted successfully!');
                setValue('title', '');
                setValue('content', '');
                setValue('category_id', '');
                setValue('image', null);
            } else {
                const errorMessage = await response.text();
                throw new Error(errorMessage || 'Failed to submit post');
            }
        } catch (error) {
            console.error('Error submitting post:', error);
            alert('Failed to submit post. Please try again.');
        }
    };

    return (
        <main className="p-6 bg-white" style={{ width: '427px', height: 'auto' }}>
              <form onSubmit={handleSubmit(onSubmit)} className="space-y-4 h-full">
        <h1  className='font-bold mb-1 mt-1  text-gray-700'>Post a blog post</h1>
        <label  className="block mb-1  text-gray-700 " htmlFor="title">Title:</label>
        <input id="title"  placeholder='title' type="text" {...register('title', { required: true })} className="w-full  bg-slate-50 border-gray-300 rounded-md py-2 px-3 focus:outline-none focus:border-blue-500" />
        {errors.title && <span className="text-red-500">Title is required</span>}
        <label className="block mb-1  text-gray-700" htmlFor="category">Category:</label>
        <select id="category" {...register('category_id', { required: true })} value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)} className="w-full border-gray-300 rounded-md py-2 px-3 focus:outline-none focus:border-blue-500  text-gray-700">
            <option value="" disabled>Select Category</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
        </select>
        {errors.category_id && <span className="text-red-500">Category is required</span>}
        <label className="block mb-1  text-gray-700">Image:</label>
        <input className= "block mt-1  text-gray-700 " type="file" accept="image/*" {...register('image', { required: true })}  />
        {errors.image && <span className="text-red-500">Image is required</span>}
        <label htmlFor="content" className="block mb-1  text-gray-700">Content:</label>
         <textarea id="content" className="block mb-1  bg-slate-50"
        {...register('content', { required: true })} style={{ minHeight: '194.5px',  width:'100%'}}  ></textarea>
        {errors.content && <span className="text-red-500">Content is required</span>}
        <Button className='hover:bg-opacity-50 transition ease-out duration-300' type="submit">Submit</Button>
    </form>
        </main>
    );
}

export default AddBlogs;
