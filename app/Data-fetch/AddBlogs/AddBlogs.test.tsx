import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import AddBlogs from "./addBlogs";

const fetchMock = jest.fn();
// Setup function
function setup(jsx: React.ReactNode) {
  return render(jsx);
}
describe("<AddBlogs />", () => {
it('Testing that the component is rendered correctly', () => {
    render(<AddBlogs />);
    // Check for the heading
    expect(
      screen.getByRole('heading', { name: 'Post a blog post' })
    ).toBeInTheDocument();
    // Check for the title input field
    expect(
        screen.getByRole('textbox', { name: /Title:/i })
      ).toBeInTheDocument();
  // Check for the category select field
    expect(
      screen.getByRole('combobox', { name: /Category/i })
    ).toBeInTheDocument();
  // Check for the content textarea
    expect(
        screen.getByRole('textbox', { name: /Content/i })
        ).toBeInTheDocument();
 //Check for the submit button
   expect(
   screen.getByRole('button', { name: /Submit/i })
  ).toBeInTheDocument();
});

  it("should not submit the form if required fields are empty", async () => {
    // Arrange
    setup(<AddBlogs />);
    
    // Act
    fireEvent.click(screen.getByRole("button", { name: /Submit/i }));

    // Assert
    await waitFor(() => {
      expect(screen.getByText("Title is required")).toBeInTheDocument();
      expect(screen.getByText("Content is required")).toBeInTheDocument();
      expect(screen.getByText("Category is required")).toBeInTheDocument();
      expect(screen.getByText("Image is required")).toBeInTheDocument();
    });

  });

// Check input fields together with their associated labels if exists in the document

  it("should render all form fields", () => {
    render(<AddBlogs/>);
    expect(screen.getByLabelText("Title:")).toBeInTheDocument();
    expect(screen.getByLabelText("Category:")).toBeInTheDocument();
    expect(screen.getByLabelText("Content:")).toBeInTheDocument();
  });

});