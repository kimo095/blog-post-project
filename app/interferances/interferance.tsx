import { ReactNode } from 'react';
export interface Blog {
    id: number;
    img_url: string;
    title: string;
    content: string;
}
export interface BlogCardProps {
    id: number;
    img_url?: string;
    title: string;
    content: string;
    useSpecialStyle?: boolean;
}

export interface PaginationProps {
    currentPage: number;
    totalPages?: number | undefined; // Accepts either a number or undefined
    onPageChange: (pageNumber: number) => void;
    // totalItems:number;
    // itemsPerPage:number;
}


export interface PostFormData {
    title: string;
    content: string;
    category_id: string;
    image: FileList | null;
    handleSubmit: (data: PostFormData) => Promise<void>;
}
export interface GetBlogsProps {
    currentPage: number;
    blogs: Blog[]; 
    setBlogs: React.Dispatch<React.SetStateAction<Blog[]>>;
    setLastPage?: React.Dispatch<React.SetStateAction<number>>;
    onLastPageChange?: (lastPage: number) => void;
    lastPage?: number;
    useSpecialStyle?: boolean; 
    showSearchBar?: boolean;
}
